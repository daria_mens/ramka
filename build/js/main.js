openSearch();
mobMenu();
openCart();
function openSearch() {
    $('.search').click(function(e) {
        e.stopPropagation();
    });
    $('.search').click(function() {
        $('.form-search').addClass('open-search');
        e.stopPropagation();
    });

    $(document).click(function() {
        $('.form-search').removeClass('open-search');
    });
}

function openCart() {
    $('.ic-cart').mouseover(function() {
        $('.block-add-product').addClass('open-cart-block');
    });

    $(document).mouseout(function() {
        $('.block-add-product').removeClass('open-cart-block');
    });
}
forgetPassword();
function forgetPassword(){
    $('.forget-pass').on('click', function () {
        $('.user-form').addClass('form-invis');
        $('.user-forget').removeClass('forget-invis');
    });
    $('.btn-login').on('click', function () {
        $('.user-form').removeClass('form-invis');
        $('.user-forget').addClass('forget-invis');
    });
}



// Modal Window
var modal = (function() {

	// Cache
	var $modal = $('.modal')
	var $btn = $('.openModal')
	var $close = $modal.find('.modal_close')

	// Events
	$btn.on('click', showModal);
	$modal.on('click', hideModal); 

	// Functions
	function showModal(e){
		var id = $(this).attr('data-modal');
		$(id).addClass('show');

	}

	function hideModal(e){
		if ($(e.target).hasClass('modal_close') || 
			$(e.target).hasClass('modal')) {
			$modal.removeClass('show');
		}
	}

	return {
		modals: $modal
	}

})();

// Open mobile menu
function mobMenu() {
    $('.button').on('click', function(){
        $('.nav').toggleClass("opened");
        $('.button-toggle').toggleClass('nav-close');
        $('.ic-menu svg').toggleClass('invis-mob');
    });
}


// open mobile dropdown
$('.dropdow-menu').on('click', function(){
    $('.dropdown').toggleClass("open-dropdown");
});




// Sliders
bigSlider();
//Main slader
function bigSlider() {
    var mainSlider = new Swiper('.main-slider .swiper-container', {
        nextButton: '.slide-button-next',
        prevButton: '.slide-button-prev',
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 0,
        autoplay: 5500,
        speed:1000,
        loop: true
    });
}


$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $(".nav-container").addClass("scroll-bg");
    } else {
        $(".nav-container").removeClass("scroll-bg");
    }
});



// Show message Product add in cart
$('.btn-product-card').on('click', function() {
        showProductMessage(this);
});

function showProductMessage(elm){

    var addProductEl = $(elm).closest('.product-card').find('.add-cart');
    addProductEl.addClass('show-product');

        setTimeout(function () {
            addProductEl.removeClass('show-product')
        }, 2000);
}



// Product slider index.html

var swiper = new Swiper('.product-offers-slider .swiper-container', {
    pagination: '.swiper-pagination',
    slidesPerView: 4,
    centeredSlides: false,
    paginationClickable: true,
    spaceBetween: 30,
    autoplay: 5500,
    speed:1000,
    loop:true,
    breakpoints: {
        1200: {
            slidesPerView:3,
            spaceBetween: 40
        },
        1000: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        570: {
            slidesPerView: 1,
            spaceBetween: 20
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10
        }
    }

});

$.stellar();


// Parteners
var parteners = new Swiper('.parteners .swiper-container', {
    nextButton: '.parteners .swiper-button-next',
    prevButton: '.parteners .swiper-button-prev',
    slidesPerView: 8,
    spaceBetween: 100,
    loop:true,
    autoplay: 3500,
    speed:1000,
    breakpoints: {
        1024: {
            slidesPerView: 6,
            spaceBetween: 40
        },
        768: {
            slidesPerView: 5,
            spaceBetween: 30
        },
        640: {
            slidesPerView: 4,
            spaceBetween: 20
        },
        320: {
            slidesPerView: 2,
            spaceBetween: 10
        }
    }
});
imgHover();
// change img src on hover

function imgHover(){
    $('.product-card').on({
        mouseover : function() {
            $(this).children('.product-cart-img').find('img').attr('src','../img/frames/frame-angle.jpg');
            $(this).children('.product-cart-img').find('img').fadeIn(1000);
        },
        mouseout : function() {
            $(this).children('.product-cart-img').find('img').attr('src','../img/frames/frame2.jpg');
            $(this).children('.product-cart-img').find('img').fadeIn(1000);
        }
    });

}
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("btn-active");
        this.nextElementSibling.classList.toggle("show-accordion");
    }
}

// BUTTON FOR INPUT amount

$(document).ready(function() {
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
});

$(document).ready(function(){
    $('.ex1').zoom();
});

var gif = new Swiper('.img-gif .swiper-container', {
    paginationClickable: true,
    autoplay: 3500,
    speed:1000,
    spaceBetween: 30,
    effect: 'fade'
});

var singleProduct = new Swiper('.single-product .swiper-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: 4500,
    autoplayDisableOnInteraction: false
});

    $(".close").on("click", function(){
        $(this).closest(".add-product-container").remove();
    });
showColor();
function showColor() {
    $('.all-color').on('click', function(e){
        $('.all-clr-block').addClass('all-clr-vis');
        e.stopPropagation();
    });


    $('.close-clr').on('click', function() {
        $('.all-clr-block').removeClass('all-clr-vis');
    });

    $('body').on('click', function(e) {
        e.stopPropagation();
        $('.all-clr-block').removeClass('all-clr-vis');
    });
    $('.close-clr').on('click',function(e) {
        e.stopPropagation();
    });

}



$('.btn-categ').on('click',function(){
    if($('.categ-hid').hasClass('categ-open')){
        $('.categ-hid').removeClass('categ-open');
        $('.ic-close-filter').css({'opacity': '0', 'visibility': "hidden"});
        $('.ic-open-filter').css({'opacity': '1', 'visibility': "visible"});
        $('body').css('overflowY' , 'auto');
    } else {
        $('.categ-hid').addClass('categ-open');
        $('body').css('overflowY' , 'hidden');
        $('.ic-close-filter').css({'opacity': '1', 'visibility': "visible"});
        $('.ic-open-filter').css({'opacity': '0', 'visibility': "hidden"});
    }

});