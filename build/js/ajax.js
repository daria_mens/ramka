$('.load-more').on('click', function() {
    var btnLoad = $(this);
    $.ajax({
        type: 'GET',
        url: '../data/guide-item.html',
        beforeSend: function(){
            btnLoad.addClass("loaded");
        },
        success: function(resp) {
            btnLoad.removeClass("loaded");
            showResults(resp);
        },
        error: function() {
            alert( "Request failed: :'(" );
        }
    });
});

function showResults(data){
    $('.row-product-guide').append(data).show(2000);
}